<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');

	//validate Login
	$permissionsRequired=1;
  require_once('etc/login_check.php');

	$request=array();
	$request['view']='equipment.showEquipment';

	//new entry via showEquipment
	if(isset($_POST['newEntry'])){
		$request['newEntry']=['name'=>$_POST['name'], 'category'=>$_POST['category'],
													'username'=>$_POST['username'], 'count'=>$_POST['count']];
	}

	//editbutton
  if(isset($_POST['editEquipment'])){
		$request['view']='equipment.editEquipment';
		$request['id']=$_POST['id'];
		if(isset($_POST['equipmentName'])){
			$request['updateEquip']=['name'=>$_POST['equipmentName'], 'category'=>$_POST['category'], 'id'=>$_POST['id']];
		}
		if(isset($_POST['username'])){
			$request['updateOwner']=['username'=>$_POST['username'], 'count'=>$_POST['count'], 'id'=>$_POST['id']];
		}
	}

	if(isset($_POST['deleteEquip'])){
		$request['deleteEquip']=$_POST['deleteEquip'];
	}

	if(isset($_POST['showEquipmentList'])){
		$request['view']='equipment.showEquipmentList';
	}

  if(isset($_POST['editEquipmentList'])){
		$request['view']='equipment.editEquipmentList';
	}

	if(isset($_GET['category'])){
		$request['catSort']=$_GET['category'];
	}else{
		$request['catSort']='';
	}

	//get user permissions from login check
	$request['permissions']=$perms;
	$equipmentController=new Controller($request);
	echo $equipmentController->display();
 ?>
