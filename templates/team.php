<!-- parallex-->
<div class="parallax-container top" style="height: 100vh; width: 100vw; ">
	<div class="parallax-item 30">
		<img id="Main-Pic" src="pics/Start-Frame/Team-BG.jpg" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>

	<div class="parallax-item 50">
		<h1 id="Team-Titel">TEAM</h1>
	</div>

	<div class="parallax-item 65">
		<img id="Main-Pic" src="pics/Start-Frame/Team-VG.png" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>
</div>

<!-- content -->

<div class="interval-backdrop" id="Interval-Res-Back"></div>
<div class="interval" id="Interval-Res">
	<section id="Team-sec">
		<?php
			if ($this->_['team']) {
				//print out every member
				foreach($this->_['team'] as $row) {
					if($row['pos']!=0){		
						//all roles as one string
						$funktion="";
						foreach($row['funktion'] as $temp){
							$funktion.=($funktion!='')?", ":null;
							$funktion.=$temp;
						}
						//replace last comma
						if(strpos($funktion, ',')!==false){
			        $funktion=substr_replace($funktion, ' und ', strrpos($funktion, ','), 1);
			      }

						echo '<div class="team-mitglied" onclick="showText()">
									<div class="team-text" onclick="hideText()">
									<h3 class="team-h3">' . $row['vorname'] . " ". $row['name'] . '</h3>
									<p class="team-p">' . str_replace('[role]', $funktion, $row['beschreibung']) .'</p>';

						//insta and yt link
						echo !empty($row['instalink'])? '<a href="'.$row['instalink'].'" target="_blank"><img class="team-img" src="pics/Logos/IGL.png" alt=""></a>':null;
						echo !empty($row['youtubelink'])? '<a href="'.$row['youtubelink'].'" target="_blank"><img class="team-img" src="pics/Logos/YTLogo.png" alt=""></a>':null;

						//picure
						echo '</div>
										<img class="team-div-img" src="pics/Profile-Pictures/'.$row['username'].'.jpg" alt="hier müsste ein Lappen sein">
									</div>';
					}
				}
			}else{
				echo 'Daten konnten nicht geladen werden';
			}
		?>
	</section>
</div>
