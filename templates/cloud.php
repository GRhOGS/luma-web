
<?php
  //dont show error messages (can't jump to deleted post after reload)
  error_reporting(E_ERROR | E_PARSE);
  $path=$this->_['path'];

  //upload success/failure messages
  if (isset($this->_['uploadSucc'])) {
    if(!$this->_['uploadSucc']){
        echo '<script>alert("Upload fehlgeschlagen :|");</script>';
    }else{
      echo '<p style="color: var(--einf); margin-left: 1vw; font-family: calibri;">Datei erfolgreich hochgeladen :}</p>';
    }
  }

  //folder creation success/failure messages
  if (isset($this->_['folderSucc'])) {
    if(!$this->_['folderSucc']){
        echo '<script>alert("Ordner konnte nicht erstellt werden :|");</script>';
    }else{
      echo '<p style="color: var(--einf); margin-left: 1vw; font-family: calibri;">Ordner erfolgreich erstellt :}</p>';
    }
  }

  //file delete success/failure messages
  if (isset($this->_['deleteSucc'])) {
    if(!$this->_['deleteSucc']){
        echo '<script>alert("Datei konnte nicht gelöscht werden :|");</script>';
    }else{
      echo '<p style="color: var(--einf); margin-left: 1vw; font-family: calibri;">Datei erfolgreich gelöscht :}</p>';
    }
  }
?>
<!-- __________ storage bar __________ -->
<div id="Wrap">

    <div id="Search-Div">
      <form action="?" method="GET">
        <input placeholder="Search" type="text" name="search" value="<?php echo isset($this->_['search'])?$this->_['search']:''; ?>">
      </form>
    </div>

    <!-- ______________print all folders___________ -->
    <div id="Foldersection">
      <h2> Ordner </h2>
      <div id="Folders">
      <?php
        /* -------- show folders --------- */
        $folders = glob('file_storage/'. $path . '*' , GLOB_ONLYDIR);
        $pos=0;
        echo "<table>";

        foreach($folders as $folder){
          $foldername=str_replace('file_storage/'. $path, '', $folder);   // only name of folder
          $shortpath=str_replace('file_storage/', '', $folder);           // path and foldername

          //individual folder-button
          if($pos==0) echo "<tr>";
          echo "<td><form action='?' method='GET'><button class='folder' type='submit' name='path' value='";
          echo $path!=''?$shortpath.'/':$foldername.'/';
          echo "'>". $foldername ."</button></form></td>";

          //line break
          $pos++;
          if($pos==9){
            $pos=0;
            echo "</tr>";
          }
        }
        echo "</tr></table>";
      ?>
      </div>
    </div>
    <div id="Storage">
    <?php
      //size of all files
      function folderSize($dir){
        $size = 0;
        foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : folderSize($each);
        }
        return $size;
      }

      $bytesize=folderSize('file_storage/');
      $maxsize=161061191137;  //max size in byte
      //convert from bytes to MB/GB
      if($bytesize < 1073741824){
        $size=round($bytesize/1048576, 3);
        $unit="MB";
      }else{
        $size=round($bytesize/1073741824, 3);
        $unit="GB";
      }
      echo "<p>". $size." ".$unit." insgesamt verbraucht von 150GB </p>";

      $width='94vw';
      $height='5px';
      $border='0';

      $progresswidth=(($width-2*$border)*$bytesize/$maxsize);
      $progress=$progresswidth>5?$progresswidth.'px':'5px';

      echo "<svg width='".$width."' height='".$height."'>
              <rect width='".$width."' height='".$height."' style='fill:var(--einf);stroke-width:".$border.";stroke:black' />
              <rect x='".$border."' y='".$border."' width='".$progress."' height='".($height-2*$border)."' style='fill:var(--del);stroke-width:".$border.";' />
            </svg>";

     ?>
    </div>
    <!-- ___________ all files ____________ -->
    <div id="Datasection">
      <h2> Dateien </h2>
        <div id="Data">
        <?php

          $pos=0;
          $imageExtensions=['png', 'jpg', 'jpeg', 'gif'];
          $videoExtensions=['mp4', 'wmv'];
          $audioExtensions=['mp3', 'wav', 'acc'];

          echo "<table>";
          $c = 0;
          //individual files
          foreach($this->_['files'] as $file){
            //check if file is private
            if(!($file['private']==true && $file['owner']!=$_SESSION['username'])){
              if($pos==0) echo "<tr>";

              //preview
              echo "<td><div id='dataSHOW-".$c."'class='datei ' > <div class='kreuz' onclick='openfile(\"close\")'><div><div></div></div></div>";
              if(in_array($file['extension'], $imageExtensions)){
                echo "<div class='imgdiv' onclick='openfile(".$c.")' ><img src='file_storage/".$file['path'].$file['fullname']."' alt='falls der Preview/Download nach einer Suche nicht funktioniert, bitte manuell ins angegebene Verzeichniss navigieren und erneut probieren :)' width='400px' ></div>";
              }else if(in_array($file['extension'], $videoExtensions)){
                echo "Video<br>";
              }else if(in_array($file['extension'], $audioExtensions)){
                echo "Musik<br>";
              }else if($file['extension']=='pdf'){
                echo "Pdf<br>";
              }else{
                echo "Datei<br>";
              }

              //highlight searched term in file name preview
              if(isset($this->_['search'])){
                $filename=$file['name'].'.'.$file['extension'];
                $highlightPos = strpos($filename, $this->_['search']);
                $filename = substr_replace($filename, '<mark>', $highlightPos, 0);
                $filename = substr_replace($filename, '</mark>', $highlightPos+strlen($this->_['search'])+strlen('<mark>'), 0);
                echo '<br>'.$filename;
                if(isset($file['path'])){
                  echo ' in '. $file['path'];
                }else{
                  echo ' im Hauptverzeichniss';
                }
              }else{
                echo '<div class="datatext" onclick="openfile('.$c.')"><p>'.$file['name'].'.'.$file['extension']. '</p></div>';
                //</div>
              }
    ?>

    <?php
  //  echo '<div class="popup DATA'.$c.'" id="Popupbox">';
            //popup
            echo "<form action='cloud?path=".str_replace('/', '%2F', $path)."' method='POST'>
                    <p class='datadescription'>
                      ".$file['path'].$file['name'].'.'.$file['extension']." [".
                      date("d.m.y h:i", filectime("file_storage/".$file['path'].$file['fullname'])).
                      "] von ".$file['owner']."
                    </p>
                    <input type='hidden' name='fullName' value='".$file['path'].$file['fullname']."'>";

            if($file['owner']==$_SESSION['username']){
              echo "<input type='text' name='name' value='".$file['name']."'><br>
                    <input type='checkbox' name='private' ".($file['private']?'checked':'').">[privat]<br>
                    <input type='hidden' name='path' value='".$file['path']."'>
                    <input type='hidden' name='extension' value='".$file['extension']."'>
                    <input type='hidden' name='owner' value='".$file['owner']."'>
                    <button type='submit' name='changeFile'>Änderungen übernehmen</button>";
            }else echo "<input type='hidden' name='name' value='".$file['name']."'>";

            echo   "<button type='submit' name='downloadFile'>herunterladen</button>
                    <button type='submit' name='deleteFile'";
                    ?> onclick="return confirm('Sicher, dass du die Datei löschen möchtest?');" <?php
            echo    ">löschen</button>
                  </form></td>";

            //line break
            $pos++;
            if($pos==5){
              $pos=0;
              echo "</tr>";
            }
          }
          $c++;
        }
        echo "</tr></table>";
      ?>
  <!--  </div>-->
  </div>

  <!-- ___________new file/folder buttons____________  -->
  <div id="Menage-Buttons">
    <div <?php if(!($unit=="GB" && $size>150)) echo 'visibility="hidden"';?>>
    <!-- __upload__ -->
    <!-- upload button sicher sticky unten rechts???-->
    <div data-popup-target='#Uploadpopup' id="upload-btn" class="btn "> <img src="pics/logos/hochladen.png" alt=""> </div>
    </div>
    <!-- folder button sichi sticky unten rechts??? -->
    <div data-popup-target='#Folderpopup' id="folder-btn" class="btn" onclick="show('File')"><img src="pics/logos/add-folder.png" alt=""> </div>

<div id="Overlay"></div>

    <!-- upload form -->
    <div  class="uploadpopup popup" id="Uploadpopup">
    <div class="popup-header">
      <div style="display: table;">
        <h2> Neue Datei erstellen </h2>
      </div>

      <button data-popup-close class="close-popup-button btn btndel">&times;</button>
    </div>
    <form action="cloud?path=<?php echo str_replace('/', '%2F', $path); ?>" method="POST" enctype="multipart/form-data">
        <!-- file -->
        <div id="drop-tn" class="drop-zone">
          <span class="drop-zone__prompt">Datei hier rein droppen oder klicken zum uploaden</span>
          <input type="file" name="file" class="drop-zone__input">
        </div>
        <!-- set as private checkbox -->
        <div class="privcheck">
          <p>Datei als privat setzen: </p>
          <input type="checkbox" name="private">
          <!-- submit -->
          <input class="btn btneinf" type="submit" value="hochladen" name="upload">
        </div>


            </form>
    </div>
    <!-- __new folder__ -->

    <!-- upload form -->
    <div class="folderpopup popup" id="Folderpopup">
      <div class="popup-header">
        <h2> Neuen Ordner erstellen </h2>
        <button data-popup-close class="close-popup-button btn btndel">&times;</button>
      </div>

      <form action="cloud?path=<?php echo str_replace('/', '%2F', $path); ?>" method="POST">
        <div class="privcheck">
        <!-- title -->
        <input type="text" name="name" placeholder="äußerst kreativer Name">
        <!-- submit -->
        <input class="btn btneinf" type="submit" value="hinzufügen" name="newFolder">
          </div>
      </form>

    </div>

  </div>

  </div>
</div>

 <script type="text/javascript" src="js/dragndrop.js"></script>
 <script type="text/javascript" src="js/popup.js"></script>
 <script type="text/javascript">
 window.onload = function(){
   //if media is edited, scroll to edited media
    if (location.hash === <?php echo '"' . $id . '"';?>) {
 		 goto(<?php echo "'" . $id . "'";?>, this);
    }
 	 window.scrollBy(0,-200);
 }

function openfile(a){
  var data = document.getElementById(String("dataSHOW-"+a));
  var openn = document.getElementsByClassName("clicked");
  var open = openn[0];

if (open == undefined) {
  data.classList.replace('datei', 'clicked');
}else if (a == "close") {

  open.classList.replace('clicked', 'datei');
}else{
  open.classList.replace('clicked', 'datei');
  data.classList.replace('datei', 'clicked');
}


}
</script>
