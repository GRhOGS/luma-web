<!-- paralax -->
<div class="parallax-container top" style="height: 100vh; width: 100vw; ">
	<div class="parallax-item 20">
		<img id="Main-Pic" src="pics/Start-Frame/Videos-BG.jpg" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>

	<div class="parallax-item 40">
		<h1 id="Videos-Titel">VIDEOS</h1>
	</div>

	<div class="parallax-item 70">
		<img id="Main-Pic" src="pics/Start-Frame/Videos-VG.png" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>
</div>

<!-- content -->
<div class="interval-backdrop" id="Interval-Res-Back"></div>
<div class="interval" id="Interval-Res">
	<div id="Video-sects">
		<!-- set Video -->
		<?php
			$video = $this->_['vid'];
			$mitglieder = $this->_['mitglied'];
			$suggestFav="157";

			if(in_array($suggestFav, array_keys($video))){
				$suggest=$suggestFav;
			}else{
				$suggest=array_keys($video)[0];
			}

			if(isset($_GET['setvid'])){
				$suggest=$_GET['setvid'];
			}
		?>
		<!-- suggested video -->
		<div id="Main-Vid-Sec">
			<div id="Main-Video-Text">
				<h1><?php echo $video[$suggest]['titel']; ?></h1>
				<p style="display:inline">
					<?php
						$besch = nl2br($video[$suggest]['beschreibung']);
						$besch = str_replace('Malle´sTudios', '</p><a href="https://mallestudios.fame-go.de/" target="_blank">Malle´sTudios</a><p style="display:inline">', $besch);
						// make all members links
						$i=0;
						foreach ($mitglieder as $mitglied) {
							if (isset($mitglied['instalink'])) {
								$besch = str_replace($mitglied['username'],
									'</p style="display: inline"><a style="display: inline" href="'.
									$mitglied['instalink'].'" target="_blank">'.$mitglied['vorname'].
									' '. $mitglied['name'].'</a><p style="display:inline">', $besch);
								}
						}
						echo $besch;
					?>
				<br><br> für mehr Infos besucht unseren
				<a target="_blank" href="https://www.youtube.com/channel/UCUjlnhaFJrJGVVocXwq9gwA">
					Youtube Kanal!
				</a></p>
			</div>
			<div id="Main-Video">
				<iframe src="<?php echo $video[$suggest]['link'];?>" frameborder="0" allow="accelerometer; autoplay=1; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>

		<!-- show video selection -->
		<div id="Select-Vid-Sec">
			<h2>aktuelle Videos:</h2>
			<form id="Selects" action="videos#Main-Video" method="GET" >
				<div class="haken">
					<select class="video-drop" name="sortVid" onchange="this.form.submit()">
						<?php View::printOptions($this->_['sortVid'],$this->_['sortVidSet']) ?>
					</select>
				</div>
				<div class="haken">
					<select class="video-drop" name="vidCount" onchange="this.form.submit()">
						<?php View::printOptions($this->_['vidCount'],$this->_['vidCountSet']) ?>
					</select>
				</div>
			</form>
			<form id="vidbtns" action="videos#Main-Video" method="GET" >
				<?php
					$path = 'media/thumbnails/';
					foreach($video as $vidID=>$row) {
						echo '<button class="dev-button grid-item" type="submit" name="setvid" value="'.$vidID.'" >
									<img class="video-button" src="'. $path.$vidID.View::getExtension($vidID, $path).'">
									</button>';
					}
				?>
				<input type="hidden" name="sortVid" value="<?php echo $this->_['sortVidSet']; ?>">
				<input type="hidden" name="vidCount" value="<?php echo $this->_['vidCountSet']; ?>">
			</form>
		</div>

		<!-- show Partners-->
		<div id="Partners-Sec">
			<h2>unsere Partner:</h2>

			<div class="partners">
				<!-- Malle -->
				<a href="https://mallestudios.fame-go.de/" target="_blank">
					<img src="pics/Profile-Pictures/Mellestudios.png" alt="" class="partner-pic">
					<h3 class="partner-text">Malle´sTudios</h3>
				</a>
				<!-- Jano -->
				<a href="https://www.youtube.com/channel/UCSMxeicGxTVj_dHF1MLN7AQ" target="_blank">
					<img src="pics/Profile-Pictures/Jano.jpg" alt="" class="partner-pic">
					<h3 class="partner-text">Jano127</h3>
				</a>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
window.onload = function(){
	//if video is selected, scroll to iframe
   if (location.hash === "Main-Video") {
		 goto('#Main-Video', this);
   }
	 window.scrollBy(0,-200);
}
</script>
