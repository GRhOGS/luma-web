<html lang="de" dir="ltr">
  <head>
    	<!-- meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-eqiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Wir zeigen Ihre Geschichte">

    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/paralax.css">

    <link rel="shortcut icon" type="image/x-icon" href="/pics/Logos/favicon.ico" />

    <!-- specific stylesheet -->

    <link rel="stylesheet" href="<?php echo 'css/style-'.$this->_['tplName'] . '.css'; ?>">

    <title>Luma Productions</title>
  </head>
  <body>
    <?php
      //get user perms
      if(isset($_SESSION['username']) && isset($_SESSION['passwort'])){
        $model=new Model();
        $permissions=$model->validateLogin($_SESSION['username'], $_SESSION['passwort']);
        unset($model);
      }
    ?>

    <div id="overflow">

      <div class="wraper">

        <!-- Header -->
        <header id="main-header">

          <!-- JS Scroll -->

          <script type="text/javascript">
          // show header backgroundColor on scroll
            function Scroll(){
              var top = document.getElementById("main-header");
              var ypos = window.pageYOffset;
              if(ypos > 100) {
                top.style.backgroundColor = "#17191aCC";
                top.style.boxShadow = "0px 0px 40px";
              }else{
                top.style.backgroundColor = "rgba(0,0,0,0)";
                top.style.boxShadow = "0px 0px 0px";
              }
            }
            window.addEventListener("scroll",Scroll);
          </script>

          <div class="container-header">
            <a class="header-logo" href="index">
              <img class="header-logo" src="pics/Logos/LOGO.png" alt="">
            </a>
            <div class="menu-wrap">
              <input type="checkbox" class="toggler">
              <div class="hamburger"><div></div></div>
              <div class="menu">
                <div>
                  <div>
                    <nav id="Nav" class="headerContent">
                      <ul>
                        <li class="nav"><a id="<?php echo(basename($_SERVER['PHP_SELF'])=='index.php')?'Selec':'nonSelec';?>"
                          href="index">Home</a>
                        </li>
                        <?php
                          // team or team edit page
                          if(isset($permissions) && $permissions>=4){
                            echo'<li class="nav"><a id="';
                            echo(basename($_SERVER['PHP_SELF'])=='teamedit.php')?'Selec':'nonSelec';
                            echo '"href="teamedit">Team</a></li>';
                          }else{
                            echo'<li class="nav"><a id="';
                            echo(basename($_SERVER['PHP_SELF'])=='team.php')?'Selec':'nonSelec';
                            echo '"href="team">Team</a></li>';
                          }
                        ?>
                        <li class="nav"><a id="<?php echo(basename($_SERVER['PHP_SELF'])=='angebote.php')?'Selec':'nonSelec';?>"
                          href="angebote">Angebote</a>
                        </li>
                        <li class="nav"><a id="<?php echo(basename($_SERVER['PHP_SELF'])=='fotos.php')?'Selec':'nonSelec';?>"
                          href="fotos">Fotografie</a>
                        </li>
                        <li class="nav"><a id="<?php echo(basename($_SERVER['PHP_SELF'])=='videos.php')?'Selec':'nonSelec';?>"
                          href="videos">Videos</a>
                        </li>
                        <li class="nav"><a id="<?php echo(basename($_SERVER['PHP_SELF'])=='music.php')?'Selec':'nonSelec';?>"
                          href="music">Musik</a>
                        </li>

                        <?php
                          // check login


                          if(isset($_SESSION['username']) && isset($_SESSION['passwort'])){
                            $model=new Model();
                            $permissions=$model->validateLogin($_SESSION['username'], $_SESSION['passwort']);
                            unset($model);

                            //equipment, perms:
                            if($permissions>=1){
                              echo'<li class="nav"><a id="';
                              if(in_array(basename($_SERVER['PHP_SELF']),['equipSelect.php','equipment.php','equipmentList.php'])){
                                echo 'Selec';
                              }else{echo 'nonSelec';}
                              echo '"href="equipSelect">Equipment</a></li>';
                            }

                            //notion, perms:
                            if($permissions>=1){
                              echo'<li class="nav"><a id="nonSelec" target="_blank" href="https://www.notion.so/6eee33868f4c485ab12cca73850c778e?v=daec1c511af04417920572d0e6450d4a">
                                    Notion</a></li>';
                            }

                            //cloud storage, perms:
                            if($permissions>=3){
                              echo'<li class="nav"><a id="';
                              echo(basename($_SERVER['PHP_SELF'])=='cloud.php')?'Selec':'nonSelec';
                              echo '"href="cloud">cloud</a></li>';
                            }

                            /*
                            //clients, perms:
                            if($permissions>=4){
                              echo'<li class="nav"><a id="';
                              echo(basename($_SERVER['PHP_SELF'])=='client.php')?'Selec':'nonSelec';
                              echo '"href="client">Klienten</a></li>';
                            }
                            */

                            //upload, perms:
                            if($permissions>=3){
                            echo'<li class="nav"><a id="';
                            echo(basename($_SERVER['PHP_SELF'])=='upload.php')?'Selec':'nonSelec';
                            echo '"href="upload">Medienverwaltung</a></li>';

                            }

                            //hash, perms:
                            if($permissions>=5){
                            echo'<li class="nav"><a id="';
                            echo(basename($_SERVER['PHP_SELF'])=='hash.php')?'Selec':'nonSelec';
                            echo '"href="hash">hash</a></li>';
                            }

                            // log off
                            echo'<li class="nav"><a id="nonSelec" href="etc/logout">'.$_SESSION['username'].' abmelden</a></li>';
                          } else {
                            echo'<li class="nav"><a id="';
                            echo (basename($_SERVER['PHP_SELF'])=='login.php')?'Selec':'nonSelec';
                            echo'"href="login">Login</a></li>';
                          }
                        ?>
                      </ul>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>

        <!-- content from template page -->
        <?php echo $this->_['content']; ?>
      </div class="wraper">

      <!-- footer -->
      <footer>
        <section>
          <div class="social-footer">
            <h3>Social Media</h3>
            <ul id="social-ul">
              <li><a target="blank" href="https://www.instagram.com/production_luma/"><img class="social-logo" src="pics/Logos/IGL.png"> @production_luma</a></li>
              <li><a target="blank" href="https://www.youtube.com/channel/UCUjlnhaFJrJGVVocXwq9gwA"><img class="social-logo" src="pics/Logos/YTLogo.png"> LUMA </a></li>
            </ul>
          </div>
          <div class="info-footer">

            <ul>
              <li><a href="angebote#Contact">Kontakt</a></li>
              <li><a href="impressum">Impressum</a></li>
            </ul>
          </div>
          <div id="Copy">
            <div id="Copy-in"></div><p id="LC">LUMA &copy; <?php echo date('Y');?></p>
          </div>
        </section>
      </footer>
    </div>
  </body>

  <script type="text/javascript" src="js/jquery-3.5.1.js"></script>
  <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
  <script type="text/javascript" src="slick/slick.min.js"></script>
  <script type="text/javascript" src="js/slide.js"></script>
  <script src="js/paralax.js"> </script>
  <script type="text/javascript">
  //adjust backdropsize automativally to foreground
  function Backdrop(){
  		var hei = document.getElementById("Interval-Res").clientHeight;
  		var dest = document.getElementById("Interval-Res-Back");
			dest.style.height = String(hei) + "px";
		}
//disable rightclickaction ob images
		$('img').mousedown(function (e) {
		  if(e.button == 2) { // right click
		    return false; // do nothing!
		  }
		});
//timeoffsett for slow performences
  function TimeBD(){
  	setTimeout(Backdrop, 500);
  };
  Backdrop();
//resize backdrop of interval on resize
  window.addEventListener('resize',TimeBD);
  // window.onresize = Backdrop;

  </script>
</html>
