<h1 id="Select-Title">Möchtest du Equipment oder Equipmentlisten einsehen/bearbeiten?</h1>

<div id="Select-Buttons">
  <!-- equipment button -->
  <button id="Equipment-btn" onclick="window.location.href='equipment'">
    <?php echo SVG::equipment(); ?>
    <p>Equipment</p>
  </button>

  <!-- equipment list button -->
  <button id="EquipmentListe-btn" onclick="window.location.href='equipmentList'">
    <?php echo SVG::equipmentList(); ?>
    <p>Equipmentlisten</p>
  </button>
</div>

<script type="text/javascript" src="js/lottie.js"></script>
<script type="text/javascript" src="js/equipment.js"></script>
