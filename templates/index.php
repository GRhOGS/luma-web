<!-- parallex -->
<?php
	$mainbild = rand(1,4);
?>
<div class="parallax-container top" style="height: 100vh; width: 100vw; ">
	<div class="parallax-item 40 hide" style="justify-content: center;display: flex;">
		<img id="Main-Pic" src="pics/Main-Pics/<?php echo $mainbild. '-BG.jpg';?>"
					style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>
	<div class="parallax-item 50 mobilebg" style="justify-content: center;display: flex;" >
		<img class="hide" src="pics/Main-Pics/Main-LOGO.png" style="height: 100vh; object-position: center;">
	</div>

	<div class="parallax-item 80 hide" style="justify-content: center;display: flex;">
		<!-- Main Bild -->

		<img src="pics/Main-Pics/<?php echo $mainbild.'-VG.png';?>" alt="" style="height: 100vh; width: 100vw; object-fit: cover;">

	</div>
</div>

<!-- Videos -->

<div class="interval-backdrop"></div>
<div class="interval">
	<section class="interval-showcase-right">
		<div class="description-text">
			<div class="description-content">
				<h1>Videos</h1>
				<p>Wir bannen Deine Idee auf Film. In höchster Qualität und mit Liebe zum Detail. Egal ob Kurzfilm, Musikvideo oder Imagefilm, kein Wunsch bleibt offen…</p>
			</div>
		</div>
		<div class="box">
			<div  id="box1" class="boxnr " onclick="location.href='videos';"><h1 class="bild-titel">Videos</h1></div>
		</div>
	</section>
</div>

<!-- Slider -->

<div class="parallax-container middle" style="height: 70vh; width: 100vw; ">
	<div class="parallax-item 80">
		<div class="slider" >
			<div class="teile teil1" ><h2>Video</h2></div>
			<div class="teile teil2" ><h2>Musik</h2></div>
			<div class="teile teil3" ><h2>Fotografie</h2></div>
		</div>
	</div>
	<div class="parallax-item 90">
		<div id="slider-content">
			<h1 id="dbw">UNSER ANGEBOT</h1>
			<a id="btn-slide" class="btn" href="angebote">besuchen</a>
		</div>
	</div>
</div>

<!-- Musik -->

<div class="interval-backdrop"></div>
<div class="interval">
	<section class="interval-showcase-left">
		<div class="box">
			<div id="box2" class="boxnl " onclick="location.href='music';"><h1 class="bild-titel">Musik</h1></div>
		</div>
		<div class="description-text">
			<div class="description-content">
				<h1>Musik</h1>
				<p>Du möchtest mit deinen eigenen Songs durchstarten? Unsere Produzenten liefern den Sound den Du willst, egal ob Live oder im Studio!</p>
			</div>
		</div>
	</section>
</div>

<!-- Team -->

<div class="parallax-container top" style="height: 60vh;">
	<div class="parallax-item 60">
		<img id="Main-Pic" src="pics/Main-Pics/Main-Pic-Blurred.jpg" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>
	<div class="parallax-item 80">
		<section id="Team">
			<div id="Team-left">
				<img class="slide-in" id="t1" src="pics/Team-Solo/dummy1.png" alt="">
				<img class="slide-in" id="t2" src="pics/Team-Solo/dummy2.png" alt="">
				<img class="slide-in" id="t3" src="pics/Team-Solo/dummy6.png" alt="">
			</div>
			<div class="mid-section-team">
				<div id="mid-section-team-1"><h1>Das Team</h1></div>
				<div id="mid-section-team-2"><a href="team" class="btn">besuchen</a></div>
			</div>
			<div id="Team-right">
				<img class="slide-in" id="t6" src="pics/Team-Solo/dummy4.png" alt="">
				<img class="slide-in" id="t5" src="pics/Team-Solo/dummy3.png" alt="">
				<img class="slide-in" id="t4" src="pics/Team-Solo/dummy5.png" alt="">
			</div>
		</section>
	</div>
</div>


<!-- Fotos -->

<div class="interval-backdrop" id="Deepestpoint"></div>
<div class="interval">
	<section class="interval-showcase-right">
		<div class="description-text">
			<div class="description-content">
				<h1>Fotos</h1>
				<p>Du möchtest mehr als Hundefilter für deinen Social Media Auftritt? Unsere Fotografen setzen alles ins richtige Licht.</p>
			</div>
		</div>
		<div class="box">
			<div id="box3" class="boxnr " onclick="location.href='fotos';"><h1 class="bild-titel">Bilder</h1></div>
		</div>
	</section>
</div>
