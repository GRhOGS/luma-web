<div class="parallax-container top" style="height: 100vh; width: 100vw; ">
	<div class="parallax-item 20">
		<img id="Main-Pic" src="pics/Start-Frame/Fotos-BG.jpg" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>

	<div class="parallax-item 40">
		<h1 id="Fotos-Titel">Fotos</h1>
	</div>

	<div class="parallax-item 70">
		<img id="Main-Pic" src="pics/Start-Frame/Fotos-VG.png" style="height: 100vh; width: 100vw; object-fit: cover;">
	</div>
</div>

<!-- content -->

<div class="interval-backdrop" id="Interval-Res-Back"></div>
<div class="interval" id="Interval-Res">

	<!-- categories -->
	<section id="Show-Cats">
		<form class="" action="fotos#Galery"  method="GET">
			<?php
				foreach ($this->_['sortFoto'] as $cat) {
					echo '<button class="cat-but" type="submit" value="'.$cat.'" name="sortFoto">';
					$foto = $this->_[$cat.'-foto'];
					$i=0;
					foreach ($foto as $key) {
						if ($i<3) {
							echo '<div class="cats-pic-'.$i.'" style="width:300px; height: 169px;">
											<img style="width: 100%; height:100%;" class="lazy" data-src="'.$key['link'].'">
										</div>';
							$i++;
						}
					}
					echo '<h2>'.$cat.'</h2></button>';
				}
			?>
		</form>
	</section>
</div>

<form class="" action="fotos#Photographers" method="GET">
	<!-- photographers -->
	<section id="Photographers">
		<h1>unsere Fotografen</h1>
		<?php
			$i = 1;
			$fotograf=$this->_['fotograf'];
			foreach ($fotograf as $key) {
				echo ' <button name="sortFotograf" type="submit" value="'.$key['username'].'" class="fotograf ';
				if (fmod($i, 2) != 0) {
					echo 'ftop" >';
				}else{
					echo 'fbtm" >';
				}
				echo'<img style="width: 100%;" class="lazy" data-src="pics/Profile-Pictures/'.$key['username'].'.jpg" >
				<div class="fotografdiv"><h2 class="fotografname">'.$key['vorname'].'</h2>
				</div>
				</button>';
				$i++;
			}
		?>
	</section>

	<div class="interval-backdrop" id="Interval-Res-Back-Bot"></div>
	<div class="interval" id="Interval-Res-Bot">
		<button id="ShowBtn" type="button" name="button" onclick="showGalery()"></button>
		<section id="Galery">
			<button class="btn" type="submit" value="Neu zuerst" name="sortFoto" onclick="showGalery()">
				<p>Auswahl zurücksetzen</p>
			</button>
			<div class="grid">
				<?php

					$x = 1;
					$fotos = $this->_['foto'];
					foreach ($fotos as $foto) {
						$data = getimagesize( $foto['link'] );

						$width = $data[0];
						$height = $data[1];


						if($width/$height >= 28/9){
							echo '<div class="grid-item grid-item--width4" onclick="showPic('.$x.')" style="height: calc((64vw * '.$height.')/'.$width.'); min-height: calc((300px * '.$height.')/'.$width.')">';
						}elseif($width/$height > 16/9) {
							echo '<div class="grid-item grid-item--width2" onclick="showPic('.$x.')" style="height: calc((30vw * '.$height.')/'.$width.'); min-height: calc((300px * '.$height.')/'.$width.')">';
						}elseif(rand(1,10)==1){
							echo '<div class="grid-item grid-item--width2" onclick="showPic('.$x.')" style="height: calc((30vw * '.$height.')/'.$width.'); min-height: calc((300px * '.$height.')/'.$width.')">';
						}elseif($width/$height > 16/9 && rand(1,4)==1){
							echo '<div class="grid-item grid-item--width3" onclick="showPic('.$x.')" style="height: calc((48vw * '.$height.')/'.$width.'); min-height: calc((300px * '.$height.')/'.$width.')">';
						}else{
							echo '<div class="grid-item" onclick="showPic('.$x.')" style="height: calc((16vw * '.$height.')/'.$width.'); min-height: calc((150px * '.$height.')/'.$width.')">';
						}
						echo	'<img class="lazy" style="100%;" data-src="'.$foto['link'].'" alt="'.$foto['titel'].'">
						</div>';
						$x++;
					}
				?>
			</div>
		</section>
	</div>
	<?php
		$x = 1;
		if(isset($fotos)){
			$length = count($fotos);
		}

		foreach ($fotos as $foto) {
			echo '<div id="IMGBACK'.$x.'" class="Img-Show-Back" onclick="hidePic('.$x.')"></div>
						<div class="l-btn'.$x.' btnlast btnhide" onclick="next('.$x.','.$length.')"></div>
						<div id="IMGSHOW'.$x.'" class="Pic-Contain">
							<img class="lazy" style="max-width: 80vw; max-height: 80vh;" data-src="'.$foto['link'].'">
							<div class="kreuz" onclick="hidePic('.$x.')">
								<div><div></div></div>
							</div>
							<div>	<p>'.$foto['titel'].' by </p><a class="fotograf-name" target="blank" href="'.$foto['instalink'].'">'.$foto['vorname'].' '.$foto['name'].'</a></div>
						</div>
						<div class="n-btn'.$x.' btnnext btnhide" onclick="last('.$x.','.$length.')"></div>
						';
			$x++;
		}
	?>
</form>
<script type="text/javascript" src="js/masonry-docs.min.js"></script>
<script type="text/javascript" src="js/lazyLoad.js"></script>
<script type="text/javascript" src="js/load-img.js"></script>
<script type="text/javascript">
imagesLoaded( grid ).on( 'progress', function() {
  // layout Masonry after each image loads
  msnry.layout();
});
//end of plugin inclution
// init Masonry
var grid = document.querySelector('.grid');

var msnry = new Masonry( grid, {
  itemSelector: '.grid-item',
  percentPosition: true,
percentPosition: true,


});



//backdrop galary
	var galery = document.getElementById('Galery');
	var btn = document.getElementById('ShowBtn');
	var int = document.getElementById('Interval-Res-Bot');
// automatically adjust hight of backdrop of Galery
	function BackdropGal(){
		var heiback = document.getElementById("Interval-Res-Bot").clientHeight;
		var intback = document.getElementById('Interval-Res-Back-Bot');
			intback.style.height = String(heiback) + "px";
		}
// show Galery
	function showGalery(){
		galery.style.height = 'auto';
		btn.style.opacity = 0;
		btn.style.display = 'none';
		int.style.height = 'auto';
		int.style.paddingBottom =  5 + 'vh';
	BackdropGal()
	}

// make whitebox appear
function showPic(a){
	var img = document.getElementById(String('IMGBACK'+a));
	var imgshow = document.getElementById(String('IMGSHOW'+a));
	var btnarrayl = document.getElementsByClassName(String("l-btn"+a));
	var btnarrayn = document.getElementsByClassName(String("n-btn"+a));
	var btnl = btnarrayl[0];
	var btnn = btnarrayn[0];
	img.style.opacity = 1;
	img.style.display = 'flex';
	imgshow.style.opacity = 1;
	imgshow.style.display = 'block';
	btnl.classList.remove('btnhide');
	btnl.classList.add('btnshow');
	btnn.classList.remove('btnhide');
	btnn.classList.add('btnshow');
}
// make whitebox disappear
function hidePic(a){
	var img = document.getElementById(String('IMGBACK'+a));
	var imgshow = document.getElementById(String('IMGSHOW'+a));
	var btnarrayl = document.getElementsByClassName(String("l-btn"+a));
	var btnarrayn = document.getElementsByClassName(String("n-btn"+a));
	var btnl = btnarrayl[0];
	var btnn = btnarrayn[0];
	img.style.opacity = 0;
	img.style.display = 'none';
	imgshow.style.opacity = 0;
	imgshow.style.display = 'none';
	btnl.classList.add('btnhide');
	btnl.classList.remove('btnshow');
	btnn.classList.add('btnhide');
	btnn.classList.remove('btnshow');
}

// skip to next picture
function next(x,l){

	if(x!=l){
		// skip to next picture if picture is not the last one
		var n = x + 1;
	}else{
		// skip to first picture if picture is the last one
		n = 1;
	}
	hidePic(x);
	showPic(n);
}

// skip to former picture
function last(x,l){
	if(x!=1){
			// skip to former picture if picture is not the first one
		var n = x - 1;
	}else{
			// skip to last picture if picture is the first one
		n = l;
	}
	hidePic(x);
	showPic(n);
}
// set Backdrop of Galery on page load
window.onload = BackdropGal();
// time Backdrop of Galery for slow browsers
function TimeBD(){
	setTimeout(BackdropGal, 500);
};
// set Backdrop of Galery on page resize
window.addEventListener('resize',TimeBD);





</script>
