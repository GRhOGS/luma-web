<?php
class Controller{
  private $request = null;
  private $template = '';
  private $view = null;

    //construct controller
    public function __construct($request){
        if($request){
          $this->view = new View();
          $this->request = $request;
          $this->template = !empty($request['view'])?$request['view']:'default';

        }
    }

    //check login, return permissions or false
    public function loginCheck(){
      //check for provided login data
      if(isset($_SESSION['passwort']) && isset($_SESSION['username'])){
        //validate password
        $model= new Model();
        $permissions=$model->validateLogin($_SESSION['username'],$_SESSION['passwort']);
        unset($model);
        return $permissions;
      } else {
        //no username/password set
        return false;
      }
    }

    //send user to login form and print error message
    public function loginError(){
      echo('<form action="../login" method="post" id="redirect_form"><input name="redirect" type="hidden" value="true"></form>');

      //auto submit form
      //quelle: https://stackoverflow.com/questions/18600772/php-html-submit-form-automatically/18600983
      ?>
      <script type="text/javascript">
        document.getElementById("redirect_form").submit(); // Here formid is the id of your form                       ^
      </script>
      <?php
    }

    //output content
    public function display(){
      $innerView = new View();
      $controllerModel = new Model();
      isset($this->request['permissions'])?$innerView->setPermissions($this->request['permissions']):null;
      switch($this->template){
         case 'team':
            // define inner view
            $innerView->setTemplate('team');

              //get data
              $team=$controllerModel->getTeam();
              $innerView->assign('team', $team);
              break;
        case 'teamedit':
           // define inner view
           $innerView->setTemplate('teamedit');

           //change stuff
           if(isset($this->request['newMember'])){
             $innerView->assign('newMember', $controllerModel->newMember($this->request['newMember']));
           }
           if(isset($this->request['deleteMember'])){
             $innerView->assign('deleteMember', $controllerModel->deleteMember($this->request['deleteMember']));
           }
           if(isset($this->request['addRole'])){
             $innerView->assign('addRole', $controllerModel->addRoleToMember($this->request['addRole']));
           }
           if(isset($this->request['editMember'])){
             $innerView->assign('editMember', $controllerModel->editMember($this->request['editMember']));
           }

           //get stuff
           $innerView->assign('team', $controllerModel->getTeam());
           $innerView->assign('roles', $controllerModel->getRoles());
           break;
           
         case 'index':
             $innerView->setTemplate('index');
             break;

         case 'videos':
               $innerView->setTemplate('videos');
               $sorts=['Neu zuerst'];
               foreach($controllerModel->getGenres('video') as $genre){
                 $sorts[]=$genre;
               }
               $innerView->assign('sortVid',$sorts);
               $innerView->assign('vidCount',['4', '8', '12', '16', 'alle']);
               //get data
               $video=$controllerModel->getMedia($this->request['sortVid'],$this->request['vidCount'], 'video');
               $innerView->assign('vid', $video);
               $innerView->assign('sortVidSet', $this->request['sortVid']);
               $innerView->assign('vidCountSet', $this->request['vidCount']);
               $mitglied = $controllerModel->getTeam();
               $innerView->assign('mitglied', $mitglied);
               break;

         case 'angebote':
             if(isset($this->request['error'])){
               $innerView->assign('error', $this->request['error']);
             }
             $innerView->setTemplate('angebote');
             break;

         case 'fotos':
             $innerView->setTemplate('fotos');
             $innerView->assign('sortFoto',$controllerModel->getGenres('foto'));
             $innerView->assign('fotoCount',['16', 'alle']);
             //get data
             $foto=$controllerModel->getFotoByFotograf($this->request['sortFoto'],$this->request['sortFotograf']);
             $innerView->assign('foto', $foto);
             $innerView->assign('sortFotoSet', $this->request['sortFoto']);

             $genres = $controllerModel->getGenres('foto');
             foreach ($genres as $genre) {
               $innerView->assign($genre.'-foto', $controllerModel->getMedia($genre, 3, 'foto'));
             }

             $innerView->assign('fotograf', $controllerModel->getTeam('Fotograf'));
             break;

         case 'music':
             $innerView->setTemplate('music');
             $innerView->assign('sortMusic',array_merge($controllerModel->getGenres('musik'),['Neu zuerst']));
             $innerView->assign('musicCount',['8', '12', '16', 'alle']);
             //get data
             $music=$controllerModel->getMedia($this->request['sortMusic'],$this->request['musicCount'], 'music');
             $innerView->assign('music', $music);
             $innerView->assign('sortMusicSet', $this->request['sortMusic']);
             $innerView->assign('musicCountSet', $this->request['musicCount']);
             break;

        case 'hash':
             $innerView->setTemplate('hash');
             break;

       case 'cloud':
            $innerView->setTemplate('cloud');
            $path=$this->request['path'];
            $innerView->assign('path',$path);
            //delete file
            if(isset($this->request['deleteFile'])){
              $uploadsucess=$controllerModel->deleteFile($this->request['deleteFile']);
              $innerView->assign('deleteSucc',$uploadsucess);
            }
            //change file desc
            if(isset($this->request['changeFile'])){
              $editsuccess=$controllerModel->editFile($this->request['changeFile']);
              $innerView->assign('editSucc', $editsuccess);
            }
            //file upload
            if(isset($this->request['newFile'])){
              $uploadsucess=$controllerModel->uploadCloudFile($this->request['newFile']);
              $innerView->assign('uploadSucc',$uploadsucess);
            }
            //file download
            if(isset($this->request['downloadFile'])){
              $controllerModel->downloadFile($this->request['downloadFile']);
            }
            //new folder
            if(isset($this->request['newFolder'])){
              $foldersucess=$controllerModel->createFolder($this->request['newFolder']);
              $innerView->assign('folderSucc',$foldersucess);
            }
            //search for specific files
            if(isset($this->request['search'])){
              $innerView->assign('search', $this->request['search']);
              $innerView->assign('files',$controllerModel->searchFiles($this->request['search']));
            }else{
              $innerView->assign('files',$controllerModel->getFilesFromDir($path));
            }

            break;

        case 'equipSelect':
           $innerView->setTemplate('equipSelect');
           break;

        case 'equipment.showEquipment':
             //delete equip
             if(isset($this->request['deleteEquip'])){
               $innerView->assign('deleted', $controllerModel->deleteEquip($this->request['deleteEquip']));
             }

             //new equip entry
             if(isset($this->request['newEntry'])){
               $innerView->assign('addSuccess', $controllerModel->newEquipment($this->request['newEntry']));
             }
             $equipment = $controllerModel->getEquipment(false, $this->request['catSort']);
             $innerView->assign('equipment', $equipment);
             $innerView->assign('usernames', $controllerModel->getUsernames());
             $innerView->setTemplate('equipment.showEquipment');
             break;

         case 'equipment.editEquipment':
             $innerView->assign('id', $this->request['id']);
             $innerView->assign('usernames', $controllerModel->getUsernames());
             //update user/equip if previously selected
             if(isset($this->request['updateEquip'])){
               $innerView->assign('updateEquip', $controllerModel->updateEquip($this->request['updateEquip']));
             }
             if(isset($this->request['updateOwner'])){
               $innerView->assign('updateOwner', $controllerModel->updateOwners($this->request['updateOwner']));
             }

             $equipment = $controllerModel->getEquipment($this->request['id']);
             $innerView->assign('equipment', $equipment);
             $innerView->setTemplate('equipment.editEquipment');
             break;

         case 'equipment.showLists':
             if(isset($this->request['deleteList'])){
               $controllerModel->deleteList($this->request['listID']);
             }
             $innerView->assign('lists', $controllerModel->getEquipmentLists());
             $innerView->setTemplate('equipment.showLists');
             break;

         case 'equipment.showEquipmentList':
             if(isset($this->request['newList'])){
               $listID=$controllerModel->newList($this->request['newList']);
             }else{
               $listID=$this->request['listID'];
             }
             if(isset($this->request['updateList'])){
               $controllerModel->updateList($listID, $this->request['updateList']);
             }
             $innerView->assign('listID', $listID);
             $innerView->assign('list', $controllerModel->getEquipmentList($listID));
             $innerView->setTemplate('equipment.showEquipmentList');
             break;

         case 'equipment.editEquipmentList':
             $innerView->assign('selected', $controllerModel->getEquipmentFromList($this->request['listID']));
             $innerView->assign('equipment', $controllerModel->getEquipment());
             $innerView->assign('listName', $this->request['listName']);
             $innerView->assign('listID', $this->request['listID']);
             $innerView->setTemplate('equipment.editEquipmentList');
             break;

         case 'login':
             if(isset($this->request['username']) && isset($this->request['passwort'])){
               $username = $this->request['username'];
               $passwort = $this->request['passwort'];

               if($controllerModel->validateLogin($username, $passwort)){
                 echo"<head><meta http-equiv='refresh' content='0; URL=index'></head>";
               } else {
                 Self::loginError(false);
               }
             } else {
                 $innerView->setTemplate('login');
             }
             break;

         case 'client':
             $seperators=['--- Videos ---', '--- Musik ---', '--- Bilder ---'];
             if(isset($this->request['newEntry'])){
               $controllerModel->addClient($this->request['newEntry'], $seperators);
             }
             if(isset($this->request['updateClient'])){
               $controllerModel->updateClient($this->request['updateClient'], $seperators);
             }
             if(isset($this->request['deleteClient'])){
               $innerView->assign('deleted', $controllerModel->deleteClient($this->request['deleteClient']));
             }
             $innerView->assign('clients', $controllerModel->getClient());
             $projects=array_merge([['titel'=>$seperators[0]]], $controllerModel->getMedia("Neu zuerst", "alle", "video"),
                                   [['titel'=>$seperators[1]]], $controllerModel->getMedia("Neu zuerst", "alle", "audio"),
                                   [['titel'=>$seperators[2]]], $controllerModel->getMedia("Neu zuerst", "alle", "photo"));
             $innerView->assign('projects', $projects);
             $innerView->setTemplate('client');
             break;

         case 'upload':
           //delete media
           isset($this->request['deleteMedia'])?$controllerModel->deleteMedia($this->request['deleteMedia']):null;

           //update media
           isset($this->request['updateMedia'])?$controllerModel->updateMedia($this->request['updateMedia']):null;

           //add Member to media
           isset($this->request['addCreator'])?$controllerModel->updateCreatorOnMedia($this->request['addCreator']):null;

           //rempve Member from media
           isset($this->request['deleteCreator'])?$controllerModel->deleteCreatorFromMedia($this->request['deleteCreator']):null;

           //upload media
           if($this->request['allowUpload']){
             $uploaded=$controllerModel->uploadMedia($this->request['newMedia'], $this->request['file'], $this->request['thumbnail']);
             $innerView->assign('uploaded', $uploaded);
           }

           if(isset($this->request['newMedia']) && isset($uploaded) && !$uploaded){
             $innerView->assign('newMedia', $this->request['newMedia']);
           }

           //get all creators for dropdown select
           foreach($controllerModel->getTeam() as $creator){
             $creators[]= $creator['username'];
           }
           $innerView->assign('creators', $creators);

           //get all media
           if(isset($this->request['type'])){
             $innerView->assign('type', $this->request['type']);
             $innerView->assign('media', $controllerModel->getMediaPlus($this->request['type'],
                                                         $this->request['userSelect']));
           }

           $innerView->setTemplate('upload');
           break;
         case 'impressum':
         $innerView->setTemplate('impressum');
         break;
      }
      $this->view->setTemplate('frame');
      $this->view->assign('tplName', $this->template);
      $this->view->assign('content', $innerView->loadTemplate());
      return $this->view->loadTemplate();
   }
}
?>
