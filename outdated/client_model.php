//get one or all clients
public function getClient($id=false){
  //get client(s) info from DB
  $pdo = $this->connect();
  $query = "SELECT  client.client_id as client_id, vorname, name as nachname, email, firma, einzahlung,
            preis, titel, medium.medien_id as medien_id FROM client
            JOIN clienttomedium ON client.client_id=clienttomedium.client_id
            JOIN medium ON medium.medien_id=clienttomedium.medien_id";
            if($id){
              $query .= " AND client.client_id='".$id."'";
            }
            $query .= " ORDER BY firma DESC";

  $stmt=$pdo->prepare($query);
  $stmt->execute();

  //save query output
  $output=[];
  while($row =  $stmt->fetch()){
    if(array_key_exists($row['client_id'],$output)){
      $output[$row['client_id']]['debt']+=$row['preis'];
      $output[$row['client_id']]['projects'][$row['medien_id']]=['title'=>$row['titel'], 'price'=>$row['preis']];
    } else {
      $output[$row['client_id']]=['name'=>$row['vorname']." ".$row['nachname'], 'email'=>$row['email'],
                          'company'=>$row['firma'], 'paid'=>$row['einzahlung'], 'debt'=>$row['preis'],
                          'projects'=>[$row['medien_id']=>['title'=>$row['titel'], 'price'=>$row['preis']]]];
    }
  }
  return $output;
}

//add new client
public function addClient($newData, $seperators){
  //check for missing data
  $newData = self::cleanInput($newData);

  if($newData===false){
      return false;
  }else{
    if(!in_array($newData['project'], $seperators)){
      $pdo = $this->connect();
      //new client data
      $query = "INSERT INTO client (client_id, vorname, name, email, firma)
                VALUES (NULL, ?,?,?,?)";
      $stmt=$pdo->prepare($query);
      $stmt->execute([$newData['first'],$newData['last'],$newData['email'],$newData['company']]);

      //get ID's
      $query = 'SELECT medien_id FROM medium WHERE titel=?';
      $stmt=$pdo->prepare($query);
      $stmt->execute([$newData['project']]);
      $medID=$stmt->fetch()['medien_id'];
      $query = "SELECT client_id FROM client ORDER BY client_id DESC";
      $stmt=$pdo->prepare($query);
      $stmt->execute();
      $cliID=$stmt->fetch()['client_id'];

      //link client and media
      $query = "INSERT INTO clienttomedium (client_id, medien_id) VALUES (?,?)";
      $stmt=$pdo->prepare($query);
      $stmt->execute([$cliID, $medID]);
      return true;
    }else{
      return false;
    }
  }
}

//update client info
public function updateClient($updateData, $catSeperators){
  //check for missing data
  $updateData = self::cleanInput($updateData);

  if($updateData===false){
      return false;
  }else{
    //update general information
    $pdo = $this->connect();
    $query="UPDATE client SET email=?, einzahlung=? WHERE client_id=?";
    $stmt=$pdo->prepare($query);
    $stmt->execute([$updateData['email'],($updateData['paid']+$updateData['pay']),
                    $updateData['id']]);

    //delete deselected projects
    $query="SELECT medium.medien_id FROM medium
            JOIN clienttomedium ON medium.medien_id=clienttomedium.medien_id
            WHERE client_id=?";
    $stmt=$pdo->prepare($query);
    $stmt->execute([$updateData['id']]);
    while($medium = $stmt->fetch()){
      if(!in_array($medium['medien_id'], $updateData['projects'])){
        $query="DELETE FROM clienttomedium WHERE client_id=? AND medien_id=?";
        $innerSTMT=$pdo->prepare($query);
        $innerSTMT->execute([$updateData['id'],$medium['medien_id']]);
      }
    }

    //add new Project to client
    if(!in_array($updateData['newProject'], $catSeperators)){
      $query = 'SELECT medien_id FROM medium WHERE titel=?';
      $stmt=$pdo->prepare($query);
      $stmt->execute([$updateData['newProject']]);
      $medID=$stmt->fetch();
      $query = "INSERT INTO clienttomedium (client_id, medien_id) VALUES (?,?)";
      $stmt=$pdo->prepare($query);
      $stmt->execute([$updateData['id'],$medID['medien_id']]);
    }
  }
}

//delete client and media associated with client
public function deleteClient($id){
  $pdo = $this->connect();
  $query="DELETE FROM client WHERE client_id=?";
  $stmt=$pdo->prepare($query);
  if($stmt->execute([$id])){
    $query="DELETE FROM clienttomedium WHERE client_id=?";
    $stmt=$pdo->prepare($query);
    if($stmt->execute([$id])){
      //all deletes successfull
      return true;
    }
  }
  //some or all deletes not successfull
  return false;
}
