<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');

	//validate Login
	$permissionsRequired=4;
  require_once('etc/login_check.php');

	$request=array();
	$request['view']='client';

	//new client
	if(isset($_POST['addClient'])){
		$request['newEntry']=['first'=>$_POST['first'], 'last'=>$_POST['last'],'project'=>$_POST['project'],
													'email'=>$_POST['email'], 'company'=>$_POST['company']];
	}

	//edit client
	if(isset($_POST['updateClient'])){
		//save all checkboxes/amounts in array
		$projects=[];
		foreach($_POST as $key=>$row){
			if($row=='on'){
				//string replace because $_POST replaces " " with _
				$projects[]=$key;
			}
		}
		$request['updateClient']=['newProject'=>$_POST['project'],'projects'=>$projects, 'paid'=>$_POST['paid'],
													'email'=>$_POST['email'], 'pay'=>$_POST['pay'], 'id'=>$_POST['clientID']];
	}

	//delete client
	if(isset($_POST['deleteClient'])){
		$request['deleteClient']=$_POST['deleteClient'];
	}

	//get user permissions from login check
	$request['permissions']=$perms;
	$clientController=new Controller($request);
	echo $clientController->display();

 ?>
