<br>
<table>
  <tr>
    <th>Firma</th><th>Name</th><th>E-Mail</th><th>Projekt/e</th>
    <th><span style="color:var(--einf)">bezahlt</span>/<span style="color:var(--del)">Schulden</span></th><th>angleichen</th></tr>
  <!-- add new client -->
  <tr><form action='?' method='post'>
    <td><input placeholder="Firma" name='company' type='text'
              value='<?php echo isset($_POST['company'])?$_POST['company']:''; ?>'></td>
    <td><input style="width: 45%;" placeholder="Vorname" name='first' type='text'
              value='<?php echo isset($_POST['first'])?$_POST['first']:''; ?>'>
        <input style="width: 45%;" placeholder="Nachname" type='text' name='last'
              value='<?php echo isset($_POST['last'])?$_POST['last']:''; ?>'></td>
    <td><input type='text' placeholder="E-Mail Adresse" name='email'
              value='<?php echo isset($_POST['email'])?$_POST['email']:''; ?>'></td>
    <td colspan="2"><select name='project'>
      <?php
      //dropdown menu for all projects
      foreach($this->_["projects"] as $project){
        $projects[]=$project['titel'];
      }
      View::printOptions($projects);
      ?></select></td>
    <td><input class="btn btneinf" type="submit" name="addClient" value="Client hinzufügen"></form></td></tr>

    <!-- show all clients -->
    <?php
      $clients = $this->_["clients"];
      foreach($clients as $clientID=>$client){
        echo'<tr><form action="?" method="post">
            <td style="width: 10vw;">'.$client["company"].'</td>
            <td>'.$client["name"].'</td>
            <td style="width: 12vw;"><input type="text" name="email" value="'.$client["email"].'"></td>
            <td>';
        //all projects associated with client
        foreach($client["projects"] as $id=>$project){
          echo'<input type="checkbox" name="'.$id.'" checked>';
          echo '<p class="text">' .$project['title'].' ('.$project['price'].' €)</p>';
        }
        //dropdown to add new project
        echo'<select name="project">';
          View::printOptions($projects);
        echo'</select></td><td>';

        /* -----------dynamic pie chart---------- */
        //orientiert an: https://medium.com/hackernoon/a-simple-pie-chart-in-svg-dbdd653b6936
        echo '<div class="mone"><p>'. $client["paid"].'€/'.$client["debt"].'€</p><svg viewBox="-1.1 -1.1 2.2 2.2" style="transform: rotate(-0.25turn)">';
        //background
        if($client["paid"]>$client["debt"] ||$client['debt']<=0){
          $fill="var(--einf)";
        }else{
          $fill="var(--del)";
        }
        echo'<circle cx="0" cy="0" r="47.5%" fill="'.$fill.'" stroke="var(--white)" stroke-width="5%"/>';

        //slice
        if($fill=="var(--del)" && $client['paid']>0){
            $percentage=$client["paid"]/$client["debt"];
            $lArcFlag=$percentage<0.5?0:1;
            $x = cos(2*pi()*$percentage);
            $y = sin(2*pi()*$percentage);
            echo '<path d="M 0 0';      // starting point top of circle (center)
            echo ' L 1 0 ';             // line to top point
            echo ' A 1 1 0 '.$lArcFlag.' 1 '.$x.' '.$y;      // radius: A (radX)(radY)(rotation)(large arc)(sweep)(circX)(circY)
            echo ' L 0 0 ';             // line to the center
            echo '" fill="var(--einf)" stroke="black" stroke-width="1%"/>';
        }
        echo'</svg></div>'; //end of svg

        echo'</td><td>Ein-/Auszahlung: +<input type="number" class="numb" value="0" step="0.01" name="pay">€<br>
            <input type="hidden" name="clientID" value="'.$clientID.'">
            <input type="hidden" name="paid" value="'.$client["paid"].'">
            <input class="btn" type="submit" name="updateClient" value="Clienteninformationen aktualisieren"></form>';
    ?>
            <form method="post" action="?" onsubmit="return confirm('Sicher, dass du den Eintrag löschen willst?');"
    <?php
        echo'<br><button class="btn btndel" type="submit" name="deleteClient" value="'.$clientID.'">Client löschen</button></td>
            </tr></form>';
      }
     ?>
</table>
