<!-- __upload__ -->
<!-- upload button            vielleicht sticky unten rechts???-->
<div id="upload-btn" class="btn" onclick="showFile('Upload')"> + </div>

<!-- upload form -->
<div class="popup popuptext" id="fileUpload">
  <a onclick="popupHide('upload-popup')">X</a>
  <form action="cloud?path=<?php echo str_replace('/', '%2F', $path); ?>" method="POST" enctype="multipart/form-data">
    <!-- file -->
    <div id="drop-tn" class="drop-zone">
      <span class="drop-zone__prompt">Datei hier rein droppen oder klicken zum uploaden</span>
      <input type="file" name="file" class="drop-zone__input">
    </div>
    <!-- set as private checkbox -->
    <div> Datei als privat setzen: <input type="checkbox" name="private"></div>
    <!-- submit -->
    <input class="" type="submit" value="hochladen" name="upload">
  </form>
</div>


<!-- __new folder__ -->
<!-- folder button            vielleicht sticky unten rechts??? -->
<div id="folder-btn" class="btn" onclick="showFile('File')"> folder </div>

<!-- upload form -->
<div class="popuptext popup" id="upload-folder">
    <a>X</a>
  <form action="cloud?path=<?php echo str_replace('/', '%2F', $path); ?>" method="POST">
    <!-- title -->
    <input type="text" name="name" placeholder="äußerst kreativer Name">
    <!-- submit -->
    <input class="" type="submit" value="hinzufügen" name="newFolder">
  </form>
</div>
