<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');

	$request=array();
	$request['view']='login';

	if(isset($_POST['username'])){
		$request['username']=$_POST['username'];
	}
	if(isset($_POST['passwort'])){
		$request['passwort']=$_POST['passwort'];
	}

	$loginController=new Controller($request);
	echo $loginController->display();

 ?>
