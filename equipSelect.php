<?php
	session_start();	//start session for session storage
	include('classes/controller.php');
	include('classes/model.php');
	include('classes/view.php');
	include('etc/svg.php');

	//validate Login
	$permissionsRequired=1;
  require_once('etc/login_check.php');

	$request=array();
	$request['view']='equipSelect';

	//get user permissions from login check
	$request['permissions']=$perms;
	$equipmentController=new Controller($request);
	echo $equipmentController->display();

 ?>
