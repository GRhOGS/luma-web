//Teil von Lottie/bodymovin
var list = document.getElementById('EquipmentList-svg');
var btnlist = document.getElementById('EquipmentListe-btn');
var animationlist = bodymovin.loadAnimation({
  container: list,
  render: 'svg',
  loop: false,
  path: '../etc/lists/lists.json',
  autoplay: false,
  prerender: true,
});
btnlist.addEventListener("mouseenter", function () {
animationlist.play();
});

btnlist.addEventListener("mouseleave", function () {
animationlist.stop();
});

var equip = document.getElementById('Equipment-svg');
var btnequip = document.getElementById('Equipment-btn');
var animationvid = bodymovin.loadAnimation({
  container: equip,
  render: 'svg',
  loop: false,
  path: '../etc/equip.json',
  autoplay: false,
  prerender: true,
});
btnequip.addEventListener("mouseenter", function () {
animationvid.play();
});

btnequip.addEventListener("mouseleave", function () {
animationvid.stop();
});
