//Teil von Lottie/bodymovin
var bild = document.getElementById('Bild-svg');
var btnbild = document.getElementById('Bild-btn');
var animationbild = bodymovin.loadAnimation({
  container: bild,
  render: 'svg',
  loop: false,
  path: '../etc/bilder.json',
  autoplay: false,
  prerender: true,
});
btnbild.addEventListener("mouseenter", function () {
animationbild.play();
});

btnbild.addEventListener("mouseleave", function () {
animationbild.stop();
});

var video = document.getElementById('Video-svg');
var btnvideo = document.getElementById('Video-btn');
var animationvid = bodymovin.loadAnimation({
  container: video,
  render: 'svg',
  loop: false,
  path: '../etc/videos.json',
  autoplay: false,
  prerender: true,
});
btnvideo.addEventListener("mouseenter", function () {
animationvid.play();
});

btnvideo.addEventListener("mouseleave", function () {
animationvid.stop();
});
