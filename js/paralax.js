// code gütiger Weise von Tilmann bereitgestellt (2019)

var containers = document.getElementsByClassName('parallax-container');

var offtop = [];
for(i = 0; i < containers.length; i++){
  offtop.push(offset(containers[i]));
}

var offheight = [];
for(i = 0; i < containers.length; i++){
  offheight.push(containers[i].offsetHeight);
}

var screensize = window.innerHeight;
var scroll = window.scrollY;

var items = [];
for(i = 0; i < containers.length; i++){
  items.push(containers[i].getElementsByClassName("parallax-item"));
}

var percent = [];
var percents = [];

for (i = 0; i < containers.length; i++){
  percent = [];
  for (j = 0; j < items[i].length; j++){
    percent.push(-1 + parseInt(items[i][j].classList.item(1)) * 0.01);
  }
  percents.push(percent);
  percent = [];
}

var off = [];
var offs = [];

for (i = 0; i < containers.length; i++){
  if(containers[i].classList.item(1) == "top"){
    off = up(percents[i], items, i);
  }

  if(containers[i].classList.item(1) == "middle"){
    off = middle(percents[i], items, i);
  }

  if(containers[i].classList.item(1) == "bottom"){
    off = bottom(percents[i], items, i);
  }

  offs.push(off);
  off = [];
}


window.addEventListener("scroll", scrolled);
window.addEventListener("resize", resized);

scrolled();



function scrolled(){
  scroll = window.scrollY;

  for(j = 0; j < containers.length; j++){
    for(i = 0; i < items[j].length; i++){
      offset = (offtop[j] - scroll) * percents[j][i];

      offset = Math.ceil(offset + offs[j][i]);
      offset = offset.toString();
      offset = offset + 'px';

      items[j][i].style.top = offset;
    }
  }
}

function resized(){
  for(i = 0; i < items.length; i++){
    offtop[i] = containers[i].offsetTop;
  }
  scrolled();
}

function up(per, items, i){
  off = [];

  for (j = 0; j < items[i].length; j++){
    scroll1 = offtop[i];

    off.push((offtop[i] - scroll1) * per[j] * -1);
  }

  return off;
}

function middle(per, items, i){
  off = [];

  for (j = 0; j < items[i].length; j++){
    scroll1 = offtop[i] - ((screensize - offheight[i])/ 2);

    off.push((offtop[i] - scroll1) * per[j] * -1);
  }

  return off;
}

function bottom(per, items, i){
  off = [];

  for (j = 0; j < items[i].length; j++){
    scroll1 = offtop[i] - screensize + offheight[i];

    off.push((offtop[i] - scroll1) * per[j] * -1);
  }

  return off;
}


function offset(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return (rect.top + scrollTop);
}
